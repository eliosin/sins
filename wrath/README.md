![](https://elioway.gitlab.io/eliosin/sins/wrath/elio-sins-wrath-logo.png)

> Seek vengeance **the elioWay**

# wrath

A [sins](https://gitlab.com/sins) theme.

![](https://elioway.gitlab.io/eliosin/sins/wrath/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
