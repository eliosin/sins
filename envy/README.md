![](https://elioway.gitlab.io/eliosin/sins/envy/elio-sins-envy-logo.png)

# envy

Mobile focused, **innocent** theme.

![](https://elioway.gitlab.io/eliosin/sins/envy/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
