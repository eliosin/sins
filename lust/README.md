![](https://elioway.gitlab.io/eliosin/sins/lust/elio-sins-lust-logo.png)

# lust

A [sins](https://gitlab.com/sins) theme.

![](https://elioway.gitlab.io/eliosin/sins/lust/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
