![](https://elioway.gitlab.io/eliosin/sins/sloth/elio-sins-sloth-logo.png)

> Mischief for idle hands **the elioWay**

# sloth

A [sins](https://gitlab.com/sins) theme.

This theme is used for **elioWay** module documentation pages.

![](https://elioway.gitlab.io/eliosin/sins/sloth/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
