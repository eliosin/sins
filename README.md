![](https://elioway.gitlab.io/eliosin/sins/elio-sins-logo.png)

> Cardinally sinful **the elioWay**

# sins ![notstarted](/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

**sins** is a collection of **eliosin** themes for Listed items. They should generally work for any layout honouring the _hell_, _pillar_, _heaven_ layout of **eliosin** dogma.

- [sins Documentation](https://elioway.gitlab.io/eliosin/sins)

## The Seven Deadly Sins Index

- [envy](https://gitlab.com/eliosin/sins/blob/master/envy/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [gluttony](https://gitlab.com/eliosin/sins/blob/master/gluttony/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [greed](https://gitlab.com/eliosin/sins/blob/master/greed/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [lust](https://gitlab.com/eliosin/sins/blob/master/lust/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [pride](https://gitlab.com/eliosin/sins/blob/master/pride/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [sloth](https://gitlab.com/eliosin/sins/blob/master/sloth/README.html) ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental") Used for elioWay documentation

- [wrath](https://gitlab.com/eliosin/sins/blob/master/wrath/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

## Installing

```shell
npm install @elioway/sins --save
yarn add  @elioway/sins --dev
```

- [Installing sins](https://elioway.gitlab.io/eliosin/sins/installing.html)

## Nutshell

### `gulp`

### `npm run test`

### `npm run prettier`

- [sins Credits](https://elioway.gitlab.io/eliosin/sins/credits.html)

![](https://elioway.gitlab.io/eliosin/sins/apple-touch-icon.png)

## License

[HTML5 Boilerplate](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)
