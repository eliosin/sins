# Quickstart sins

- [sins Prerequisites](/eliosin/sins/prerequisites.html)
- [Installing sins](/eliosin/sins/installing.html)

## Usage

Once you have installed the sins theme, you can simply reference the css file in your html files:

```
<head>
  <link rel="stylesheet" href="path/to/envy/envy.min.css" />
</head>
```
