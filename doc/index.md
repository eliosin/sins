<aside>
  <dl>
  <dd>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
  enthrall’d</dd>
  <dd>By sin to foul exorbitant desires</dd>
</dl>
</aside>

**sins** is a collection of snappy, eliosin themes named after each of the Seven Deadly Sins. Ideal for interactive web apps with a mobile focus.

# Theme Paks

<article>
  <a href="/eliosin/sins/envy/star.png" target="_splash">
  <img src="/eliosin/sins/envy/favicoff.png"/>
  <div>
  <h4>envy</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/sins/gluttony/star.png" target="_splash">
  <img src="/eliosin/sins/gluttony/favicoff.png"/>
  <div>
  <h4>gluttony</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/sins/greed/star.png" target="_splash">
  <img src="/eliosin/sins/greed/favicoff.png"/>
  <div>
  <h4>greed</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/sins/lust/star.png" target="_splash">
  <img src="/eliosin/sins/lust/favicoff.png"/>
  <div>
  <h4>lust</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/sins/pride/star.png" target="_splash">
  <img src="/eliosin/sins/pride/favicoff.png"/>
  <div>
  <h4>pride</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/sins/sloth/star.png" target="_splash">
  <img src="/eliosin/sins/sloth/favicoff.png"/>
  <div>
  <h4>sloth</h4>
  <p>Our documentation pages use the <strong>sloth</strong> theme. Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/sins/wrath/star.png" target="_splash">
  <img src="/eliosin/sins/wrath/favicoff.png"/>
  <div>
  <h4>wrath</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

# Contributors wanted

Start contributing to eliosin and work **the elioWay** by adopting on a sinful theme.

# See also

- [eliosin/innocent](/eliosin/innocent)
- [eliosin/bushido](/eliosin/bushido)
