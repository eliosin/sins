# Installing sins

- [sins Prerequisites](/eliosin/sins/prerequisites.html)
- [Installing god](/eliosin/god/installing.html)

## Coming soon: Installing with generator-sin

Installing with eliosin's own **generator-sin** is the recommended way.

- [Installing generator-sin](/eliosin/generator-sin/installing.html)

```
yo sin:<sins_theme>
gulp
```

This builds an eliosin package in your folder with the selected theme's scss files which you can use as is, or customize and build on.

## Installing with npm or yarn

Add **god**, **eve**, **adon** and **sins** as NPM packages.

```shell
npm install @elioway/god @elioway/eve @elioway/adon @elioway/sins --save-dev
yarn add @elioway/god @elioway/eve @elioway/adon @elioway/sins --dev
```

### Add it to your compile funnel

- Locate the primary `X.scss` build file within your current project stylesheets.

  - For instance, the one targeted by your compile tools in the `gruntfile`, `gulpfile` or other.

- Add the following to your current project SASS build file:

```scss
@import "../node_modules/@elioway/sins/<sins_theme>/stylesheets/settings";
@import "../node_modules/@elioway/sins/god/stylesheets/theme";
@import "../node_modules/@elioway/sins/<sins_theme>/stylesheets/theme";
```

#### Using your own settings

- Copy the `settings.scss` file from `./node_modules/@elioway/sins/<sins_theme>/stylesheets/settings.scss`

- Paste it alongside `X.scss` into, for instance, `my_sinful_settings.scss`

- Change the SASS build file making sure your settings gets called before **sins**'s theme:

```scss
@import "my_sinful_settings";
@import "../node_modules/@elioway/god/stylesheets/theme";
@import "../node_modules/@elioway/sins/<sins_theme>/stylesheets/theme";
```

### Nah! I'll just use the dist

```html
<link
  rel="stylesheet"
  href="node_modules/@elioway/sins/<sins_theme>/dist/css/<sins_theme>.min.css"
/>
```

Add any of the theme's scripts to the bottom of the page:

```
  <script
    src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
    crossorigin="anonymous"
  ></script>
  <script src="node_modules/@elioway/sins/<sins_theme>/dist/js/adons.js"></script>
  <script src="node_modules/@elioway/sins/<sins_theme>/dist/js/main.js"></script>
</body>
```

## Contributing

GIT is also a fine way to install **sins**.

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliosin.git
cd eliosin
git clone https://gitlab.com/eliosin/sins.git
cd sins/<sins_theme>
npm i|yarn
gulp
```

# gulp watch issues

If gulp crashes while running "watch", try this shell command in Linux.

```
shell echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf sudo sysctl -p
```
