const { parallel } = require("gulp")
var exec = require("child_process").exec

function sloth(done) {
  exec(
    "gulp --gulpfile ./sloth/gulpfile.js build",
    function (err, stdout, stderr) {
      // console.log(stdout);
      // console.log(stderr);
      done(err)
    }
  )
}

const sins = parallel(sloth)

exports.build = sins
exports.default = sins
